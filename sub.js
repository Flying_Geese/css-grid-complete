var fruits = ['Apple', 'Orange', 'Passion Fruit', 'Tomato', 'Banana', ''];

var user1 = {'name': 'Michael Law', 'bio': 'I love coding so much'};
var user2 = {'name': 'John Doe', 'bio': 'Having so much fun'};
var user3 = {'name': 'Mark Zuck', 'bio': 'You can totally trust me'};

var users = [user1, user2, user3];

// for(var i = 0; i < fruits.length; i++) {
//     document.write('<h2>' + fruits[i] + '</h2>');
// }

for (var i = 0; i < users.length; i++) {
    document.write('<h2>' + users[i].name + '</h2><p>' + users[i].bio + '</p>');
}


// for (var i = 99; i > 0; i--) {
//     if (i == 1) {
//         console.log('1 crow left!');
//     } else {
//         console.log(i + ' crows on the wall');
//     }
// }


// var i2 = 99;

// while (i2 > 0) {
//     if (i2 == 1) {
//         console.log('1 crow left');
//     } else {
//         console.log(i2 + ' crows on the wall');
//     }
//     i2--;
// }